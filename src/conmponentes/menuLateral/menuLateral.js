import React, {useState, useContext} from 'react';
import { ContextTheme } from '../../App';
import './menuLateral.css'


//Este componente me contiene el Menu desplegable del costado izquierdo de la patanlla, y lo utilizo en el componente app

const MenuLateral = ({opcion1, opcion2, opcion4, opcion5, setstateMoveContainer, setstateMoveContainer2}) => {
   
   const {ColorThemeWhite, ColorThemeGrey, NightBoxShadow} = useContext(ContextTheme);
   
   //Este estado para cambiar el estilo left del menu y desplazarlo hacia la derecha o izquierda

  const [LeftMenu, setLeftMenu] = useState(null);

   
   // Esta funcion, es del evento hover del div vacio que utilizo para activar el menu y desplazar el contenedor de las cards hacia la derecha

   const ActivMain = () => {
   
    let windowSize = window.innerWidth;

    if (windowSize<700){
      setLeftMenu('0px');
      setstateMoveContainer('40px');
      setstateMoveContainer2('40px');  
    }
    else{ 
      setLeftMenu('0px');
      setstateMoveContainer('80px');
      setstateMoveContainer2('80px');
    }  
  }

  // Esta funcion, es del evento leave que utilzo para desactivar el menu y mover el contenedor de las cards hacia la izquierda como estaban antes
  
  const DesactivMain = () =>{
    setLeftMenu('-105px');
    setstateMoveContainer('0px');
    setstateMoveContainer2('0px');
  } 



  ////////// FUNCIONALIDAD BOTON MENU ////////////

  // Estado bandera para saber si el menu esta abierto o cerrado

  const [FlagOpenCloseMenu, setFlagOpenCloseMenu] = useState(false);
  

  // Esta funcion se ejecuta al hacer click en el menu y lo que hace es que si la bandera esta en false al hacer click, me lo abre y caso contrario me lo cierra.

  const BotonOpenCloseMenu = () => {
    
    if (!FlagOpenCloseMenu){
      ActivMain();
      setFlagOpenCloseMenu(true); 
    }
    else{
      DesactivMain();
      setFlagOpenCloseMenu(false);
    }
  }

  

  /////////FUNCIONES EVENTOS MOUSELEAVE Y MOUSEHOVER DEL MENU ////////////////// 
  
  // Ambas funciones solo se activan solo si no estan en pantalla mobil, para evitar que al hacer un touch en la zona donde se encuentra el activador me lo active, osea queda feo si funciona asi.

  // mouseleave: el menu solo se cierra si la pantalla no esta en tamaño mobil

  const DesactivMenu = () => {
    
    let windowSize = window.innerWidth;
    
    if (windowSize>700){
      DesactivMain();
    }
  }


  // mousehover: el menu solo se abre si la pantalla no esta en tamaño mobil

  const ActivMenu = () => {
    
    let windowSize = window.innerWidth;
    
    if (windowSize>700){
      ActivMain();
    }
  }

    return (
        <> 
         <span className="icon icon-menu" onClick={ ()=> {BotonOpenCloseMenu()} } style={{color:ColorThemeWhite}} ></span>
            <div className="activador-menu" onMouseOver={ ()=> {ActivMenu()} } ></div>
            <nav className="nav" onMouseLeave={ ()=> {DesactivMenu()} }  style={{left:LeftMenu,}}>
                <ul className="main" style={{background: ColorThemeGrey, border:NightBoxShadow}}> 
                    <li href="" className="menu-link" style={{color: ColorThemeWhite}}>
                        <span className="icon-homee icon-home" style={{color: ColorThemeWhite}}></span>
                        <p className="opciones">{opcion1}</p>
                    </li>
                    <li href="" className="menu-link">
                      <span className="icon-catalogo icon-car" style={{color: ColorThemeWhite}} ></span>
                      <p className="opciones" style={{color: ColorThemeWhite}}>{opcion2}</p>
                    </li>
                    <li href="" className="menu-link">
                        <span className="icon-contacto icon-address-card" style={{color: ColorThemeWhite}}></span>
                        <p className="opciones" style={{color: ColorThemeWhite}}>{opcion5}</p>
                    </li>
                    <li  href="" className="menu-link">
                        <span className="icon-chucherias icon-steam-square" style={{color: ColorThemeWhite}}></span>
                        <p className="opciones" style={{color: ColorThemeWhite}}>{opcion4}</p>
                    </li>    
                </ul> 
            </nav>
        </>
      
    );
  }
  
  export default MenuLateral;

          