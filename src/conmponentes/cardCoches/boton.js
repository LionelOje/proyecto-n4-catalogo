import React from 'react';
import './boton.css'


/// ESTE COMPONENTE ES EL DEL BOTON QUE UTILIZO DENTRO DE CardCOche Y SIRVE PARA ABRIEME LA CARDCUOTA ///

const Boton = ({texto,Click}) => {
    return (
        <div  className="btn-cotizacionn" onClick={()=> {Click()}}>{texto}</div>
      
    );
  }
  
  export default Boton;
  