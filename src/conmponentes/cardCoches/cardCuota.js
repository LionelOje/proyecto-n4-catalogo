import React, {useState, useContext} from 'react';
import './cardCuota.css'
import { ContextTheme } from '../../App';


 // ESTE COMPONENTE ES EL DE LA PARTE DEL LADO DE LA CUOTA QUE SE VE CUANDO SE HACE CLICK EN EL BOTON CALCULAR COUTA QUE SE ENCUENTRA EN CADA CARD DE COCHE. HAY UNA PARA CADA CARD DE AUTO Y LAS RENDERISO EN UN MAP DENTRO DEL COMPONENTE CardGrupal. CONTIENE DOS IMPUTS QUE SIRVEN PARA CALCULAR EL VALOR DE CUOTA DEL COCHE AL HACER CLICK EN EL BOTON CALCULAR.

 const CardCuota = ({visibility, scalee, zindex, ClickBotonn, ActiveFlagCuote, DesactiveFlagCuote, flagBtnCuotee, FlagOpenCuotes}) => {

  const {ColorThemeWhite, ColorThemeGrey, NightBoxShadow} = useContext(ContextTheme);

  // Este state, es para cargar los datos del imput, que el usuario pone por teclado al indicar la cantidad de cuotas en las que quiere pagar el auto
  const [cuotas, setcuotas] = useState('');

  // Este state, es para cargar los datos del imput, que el usuario pone por teclado al indicar si es miembro del club chevrolet, lo que le puede aplicar un descuento en el valor de la cuota o no
  const [sosParteDelClub, setsosParteDelClub] = useState('');

  // Este state, es para mostrar por pantalla el resultado del caluculo del valor de cuota que tiene que pagar el cliente
  const [result, setresult] = useState(null);;

  // Este state, es para manejar el estilo opacity, que hace que el resultado aparesca con efecto cuando se calcula el valor de cuota
  const [opacityResult, setopactyResult] = useState(0);



  /// CARGA DE IMPUTS///

  // Esta es la funcion Onchange, para cargar el dato de cantidad de cuotas que ingrese el usuario

  const InputChange = (e) =>{
 
    setcuotas(e.target.value);
    
  }


  // Esta es la funcion Onchange, para cargar el dato de cantidad de cuotas que ingrese el usuario

  const InputChangee = (e) =>{
   
    setsosParteDelClub(e.target.value);
  }



  /// VALIDACIONES IMPUTS ///

  //Esta funcion es para validar la parte del imput cantidad de cuotas. Lo que hice fue, como el usuario tiene que ingresar un numero de cuotas del 1 al 49, puse un for con esa cantidad de iteraciones y si en algun momento lo que ingreso el usuario coincide con el numero de iteraciones representados en i, poner una bandera en true (al principio estaba en false). Luego afuera comparo si hubo alguna coincidencia entre ese numero de iteracion i, con la cantidad de cuotas que puso el usuario (estan guardadas en el parametro cuotes). Si no hay coincidencia, osea la flag se queda en false, Tiro por alert una frase para avisarle al usuario que esta escribiendo mal y vacio el contenido de los 2 imputs y el del resultado. Me retorna un boleano que me indica si se paso la validacion o no 

  const validacionCuota = (cuotes) => { 
    
    let flagValidacionCuota = false;
    for (let i=1; i<49; i++){

      if (cuotes===i){
        flagValidacionCuota = true;
      }
    }
    if (!flagValidacionCuota){
      alert('En cantidad de cuotas debe ir un numero del 1 al 48');
      setcuotas('');
      setresult('');
    }  
    return flagValidacionCuota;
  }


 // Esta funcion es para validar el imput en el que el usuario ingresa si es miembro del club personal o no. si la respuesta de usuario guardada en el parametro de la funcion no es ni si ni no, me pone un alert que que me le dice al usuario que debe colocar si o no y me vacia el imput para que lo vuelva a escribir. al final retorno un boleano que me indica luego si se paso la validacion

 const validacionMiembroClub = (responseUsuario) => {

  let flagRespuestaUsuario = true;
  
    if (responseUsuario!='si' && responseUsuario!='no' ){
      alert('Debe colocar si o no');
      setsosParteDelClub('');
      setresult('');
      flagRespuestaUsuario = false;
    }  
      return flagRespuestaUsuario;
  } 


  ///CALCULO DE VALOR CUOTA Y FUNCION DEL BOTON DE CALCULO DE CUOTA ///

  // Esta funcion me calcula el valor de la cuota que le corresponde al usuario. Hice un for en donde puse como condicion, un array que me contiene todos los flags que me indican que card cuota esta abierta a partir del false. Una ves que encontre la esta abierta haga una condicion en la que corroboro la la respuesta del usuario al imput Miembro club es si. Si es si, guardo en la variable descuento el precio menos un descuento del 25%. Luego le mando al estado resultado (es lo que se muestra por pantalla), el descuento dividido la cantidad de cuotas. Si la respuesta es no, directamente al estado resultado le mando el precio divido la cantidad de cuotas

  const calculoValorCuota = (FlagCuotes, cuotass, respuestaUsu, setresultt) =>{
    
    let descuento 
    const precios = [789000, 855000, 875000, 1604900, 2100000, 1300000, 1200000, 2900000, 1456000, 1850000, 2199000, 1950000, 2400500, 3400000, 3500000]


    for (let i=0; i<FlagCuotes.length; i++){
      if (!FlagCuotes[i]){
        if (respuestaUsu === 'si'){
          descuento = precios[i] - ((25*precios[i])/100);
          setresultt(descuento/cuotass);
        }
        else{
          setresultt(precios[i]/cuotass);
        }
      }
    }
  }


  // Esta es la funcion que corresponde al boton de calcular cuota. Primero guardo en la variable cantCuotas, el string de numero de cuotas que ingreso el usuario pero lo paso a numero, asi luego se lo puedo pasar como parametro a la funcion de validacionCuota y el de calcular la cuota, ya que al principio esta en string y me daria error. Luego hago lo mismo con la respuesta del usuario en el input si de si es miembro del club, en este caso paso todo el string a minuscula asi se puede validar. Luego si ambas validaciones estan en true, procedo a llamar a la funcion de calculoValorCuota para hacer el calculo y que se me muestre por pantalla y tambien le aplico el efecto de opacity al resultado 

  const buttonCalcularValorCuota = () =>{
    
    let cantCuotas = parseInt(cuotas);
    let respuestaUsuario = sosParteDelClub.toLocaleLowerCase();

      
    if (validacionCuota(cantCuotas)){
      if (validacionMiembroClub(respuestaUsuario)) {
        calculoValorCuota(FlagOpenCuotes, cantCuotas, respuestaUsuario, setresult);
        setopactyResult(1);
      }    
    }
  }
    


  return (
    <>
    <div  className="container-cardCuota"  style={{display:visibility, transform:scalee, zIndex:zindex, boxShadow:NightBoxShadow, background:ColorThemeGrey}}>
      <span className="symbol-undo icon-undo2" style={{color:ColorThemeWhite}} onClick={ClickBotonn}></span>
      <form>
        <div className="contenedor-descripcioness">
          <label className="descripcion-globall" style={{color:ColorThemeWhite}}>Ingrese cantidad de cuotas. </label>  
          <input Name="CantCuotes" className="descripcionCardd tamaño-input" placeholder="N° de coutas" onChange={InputChange} value={cuotas}
            >
          </input>
        </div>
        <div className="contenedor-descripcioness2">
          <label className="descripcion-globall" style={{color:ColorThemeWhite}} >¿Es usted miembro de club Chevrolet?.</label>
          <input Name="ClubChe" className="descripcionCardd tamaño-input" placeholder="Si/No" value={sosParteDelClub} onChange={InputChangee}
            >
          </input>
        </div>
        <div className="contenedor-descripcioness3">
          <p className="descripcion-globall position-descri3" style={{color:ColorThemeWhite}}>Su valor de couta es:</p>
          <p className="descripcionCardd valor" style={{opacity:opacityResult, color:ColorThemeWhite}}>${result}</p>
        </div>
        <div  className="btn-calcu-cuo" onClick={()=> {buttonCalcularValorCuota()}}>Calcular Cuota</div>
      </form>
    </div>    
    </>  
  );
}

export default CardCuota; 


