import React, {useContext} from 'react';
import './cardCoche.css'
import Boton from './boton';
import { ContextTheme } from '../../App';


/// ESTE COMPONENTE CORRESPONDE A LA CARDS DE LOS COCHES QUE MAPEO DENTRO DEL COMPONENTE CardGrupal ///

const CardCoche = ({imagen, ActiveFlagCuote, tipo, nombre, precio, visibility, claseEfecto, scale, ClickBoton}) => {

  const {ColorThemeWhite, ColorThemeGrey, ColorThemeBlack, NightBoxShadow} = useContext(ContextTheme)
  return (
    <>
    <div  className={claseEfecto} style={{display:visibility, transform:scale, background:ColorThemeGrey, boxShadow:NightBoxShadow, border:NightBoxShadow}}>
      <img src={imagen} alt="" className="imagenCard" style={{boxShadow:NightBoxShadow}}></img>
      <div className="contenedor-descripciones ">
        <p className="descripcion-global" style={{color:ColorThemeWhite}}>Tipo de vehiculo:</p>  
        <p className="descripcionCard">{tipo}</p>
      </div>
      <div className="contenedor-descripciones">
        <p className="descripcion-global" style={{color:ColorThemeWhite}}>Nombre:</p>
        <p className="descripcionCard">{nombre}</p>
      </div>
      <div className="contenedor-descripciones">
        <p className="descripcion-global" style={{color:ColorThemeWhite}}>Precio:</p>
        <p className="descripcionCard">{precio}</p>
        <div className="posicion-boton"><Boton  texto="Valor cuota" Click={ClickBoton}/></div>
      </div>
    </div>    
    </>  
  );
}

export default CardCoche; 