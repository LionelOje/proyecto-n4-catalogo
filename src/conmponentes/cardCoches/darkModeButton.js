import React, {useContext, useState, useEffect} from 'react';
import { ContextTheme } from '../../App';
import './darkModeButton.css'


/// ESTE EL COMPONENTE DEL DARKMODE Y CONTIENE EL ICONO Y EL TEXTO CON EL NOMBRE DEL MODO QUE ESTA ELEGIDO ///
const DarkModeButton = () => {

    const {setColorThemeWhite, setColorThemeGrey, setColorThemeBlack, setNightBoxShadow, ColorThemeWhite, IconThemeButton, setIconThemeButton, TextThemeButton, setTextThemeButton, setImgInterfazTheme } = useContext(ContextTheme);
    
    //flag para manejar si el boton del dark mode me pone la app en dark mode o light mode
    const [FlagTheme, setFlagTheme] = useState (false);

    
    // ESTA FUNCION LA UTILIZO PARA CAMBIAR A DARK MODE O VISERVERSA. ME CAMBIA EL EL ICONO Y EL NOMBRE DEL BOTON DARK MODE, EL COLOR DEL BORDE DE LAS CARDS, ME CAMBIA LOS COLORES DE LOS ELEMENTOS, TAMBIEN ME CAMBIA LA IMAGEN DE LA INTERFAZ A UNA MAS OSCURA, Y ME PONE DE BOLEEAN LA FLAG PARA SABER EL TEMA QUE ESTA ACTIVADO

    const ChangeTheme = (nameMode, iconMode, border, white, grey, black, flag, img) =>{
        setTextThemeButton(nameMode);
        setIconThemeButton(iconMode);
        setNightBoxShadow(border);
        setColorThemeWhite(white);
        setColorThemeGrey(grey);
        setColorThemeBlack(black);
        setFlagTheme(flag);
        setImgInterfazTheme(img);
    }


    // ESTA ES LA FUNCION DEL BOTON DEL DARK MODE, SI LA FLAG DEL TEMA ESTA EN FALSO, ME LLAMA A LA FUNCION PARA ACTIVAR EL MODE Y LO PONE EN LIGHT Y SI LA FLAG ESTA EN TRUE ME LO PONE EN TRUE
    
    const ButtonTheme = () =>{

        (!FlagTheme)
          ? ChangeTheme('Light Mode', 'imagenes/sun.png', 'none', 'white', '#424242', 'rgb(27,27,27)', true, 'imagenes/fondo33.jpg')
          : ChangeTheme('Dark mode', 'imagenes/night.png', null, null, null, null, false, 'imagenes/fondo15.jpg'); 
    }    

    // Esto es para que si ala pantalla es tamaño mobil no se vea el texto Dark mode que esta bajo del boton 

    const [DisplayTextBoton, setDisplayTextBoton] = useState (null);

    useEffect(() => {
    
        let windowSize = window.innerWidth;
        (windowSize<700) ? setDisplayTextBoton('none') : setDisplayTextBoton('block');
      },[])

    return (
        <div className='container-theme'>
            <h1 className='tittle-theme' style={{color:ColorThemeWhite, display:DisplayTextBoton}}>{TextThemeButton}</h1>
            <img className='img-theme' onClick={()=> {ButtonTheme()}} src={IconThemeButton}></img>
        </div>
      
    );
  }
  
  export default DarkModeButton;
  