import React, {useState,useEffect} from 'react';
import CardCoche from './cardCoche'
import CardCuota from './cardCuota'
import './cardGroup.css'

  
 ///ESTE COMPONENTE ME CONTIENE Y MAPEA TODAS LAS CARDS COCHE Y CARD CUOTAS///
  
 const CardGrupal = ({ActivEffect,propMoveContainer,propMoveContainer2,display1,display2,display3,display4,display5,display6,display7,display8,display9,display10,display11,display12,display13,display14,display15,efectoCard1,efectoCard2,efectoCard3,efectoCard4,efectoCard5,efectoCard6,efectoCard7,efectoCard8,efectoCard9,efectoCard10,efectoCard11,efectoCard12,efectoCard13,efectoCard14,efectoCard15}) => {

   ///ESTADOS Y FUNCIONES PARA ABRIR Y CERRAR LAS CARDS COCHES Y CUOTE///

  // Estos estados son para manejar el cierre de las card del coche y a la apertura de la card que se ve la parte de las cuotas. Ademas incluye una bandera para saber que cardcuota se abrio. Cada uno de estos bloques corresponden a cada card

  const [ScaleCard1, setScaleCard1] = useState(null);
  const [ScaleCuote1, setScaleCuote1] = useState(null);
  const [Zindex1, setZindex1] = useState(null);
  const [FlagOpenCuote1, setFlagOpenCuote1] = useState(true);

  const [ScaleCard2, setScaleCard2] = useState(null);
  const [ScaleCuote2, setScaleCuote2] = useState(null);
  const [Zindex2, setZindex2] = useState(null);
  const [FlagOpenCuote2, setFlagOpenCuote2] = useState(true);

  const [ScaleCard3, setScaleCard3] = useState(null);
  const [ScaleCuote3, setScaleCuote3] = useState(null);
  const [Zindex3, setZindex3] = useState(null);
  const [FlagOpenCuote3, setFlagOpenCuote3] = useState(true);

  const [ScaleCard4, setScaleCard4] = useState(null);
  const [ScaleCuote4, setScaleCuote4] = useState(null);
  const [Zindex4, setZindex4] = useState(null);
  const [FlagOpenCuote4, setFlagOpenCuote4] = useState(true);

  const [ScaleCard5, setScaleCard5] = useState(null);
  const [ScaleCuote5, setScaleCuote5] = useState(null);
  const [Zindex5, setZindex5] = useState(null);
  const [FlagOpenCuote5, setFlagOpenCuote5] = useState(true);

  const [ScaleCard6, setScaleCard6] = useState(null);
  const [ScaleCuote6, setScaleCuote6] = useState(null);
  const [Zindex6, setZindex6] = useState(null);
  const [FlagOpenCuote6, setFlagOpenCuote6] = useState(true);

  const [ScaleCard7, setScaleCard7] = useState(null);
  const [ScaleCuote7, setScaleCuote7] = useState(null);
  const [Zindex7, setZindex7] = useState(null);
  const [FlagOpenCuote7, setFlagOpenCuote7] = useState(true);

  const [ScaleCard8, setScaleCard8] = useState(null);
  const [ScaleCuote8, setScaleCuote8] = useState(null);
  const [Zindex8, setZindex8] = useState(null);
  const [FlagOpenCuote8, setFlagOpenCuote8] = useState(true);

  const [ScaleCard9, setScaleCard9] = useState(null);
  const [ScaleCuote9, setScaleCuote9] = useState(null);
  const [Zindex9, setZindex9] = useState(null);
  const [FlagOpenCuote9, setFlagOpenCuote9] = useState(true);

  const [ScaleCard10, setScaleCard10] = useState(null);
  const [ScaleCuote10, setScaleCuote10] = useState(null);
  const [Zindex10, setZindex10] = useState(null);
  const [FlagOpenCuote10, setFlagOpenCuote10] = useState(true);

  const [ScaleCard11, setScaleCard11] = useState(null);
  const [ScaleCuote11, setScaleCuote11] = useState(null);
  const [Zindex11, setZindex11] = useState(null);
  const [FlagOpenCuote11, setFlagOpenCuote11] = useState(true);

  const [ScaleCard12, setScaleCard12] = useState(null);
  const [ScaleCuote12, setScaleCuote12] = useState(null);
  const [Zindex12, setZindex12] = useState(null);
  const [FlagOpenCuote12, setFlagOpenCuote12] = useState(true);

  const [ScaleCard13, setScaleCard13] = useState(null);
  const [ScaleCuote13, setScaleCuote13] = useState(null);
  const [Zindex13, setZindex13] = useState(null);
  const [FlagOpenCuote13, setFlagOpenCuote13] = useState(true);

  const [ScaleCard14, setScaleCard14] = useState(null);
  const [ScaleCuote14, setScaleCuote14] = useState(null);
  const [Zindex14, setZindex14] = useState(null);
  const [FlagOpenCuote14, setFlagOpenCuote14] = useState(true);

  const [ScaleCard15, setScaleCard15] = useState(null);
  const [ScaleCuote15, setScaleCuote15] = useState(null);
  const [Zindex15, setZindex15] = useState(null);
  const [FlagOpenCuote15, setFlagOpenCuote15] = useState(true);
  
  

  // Esta funcion utiliza los estados anteriores para cerrrar la card coche y abrir la card cuota, y tambien me pone en false la bandera que corresponde a la card que se hizo click. Tambien hace lo mismo pero en inversa segun si la flagOpencuote esta en true o false, que es si la card cuota esta abierta o cerrada

  const OpenCloseCuote = (setScaleCard,setScaleCuote,setZindex,setFlagOpenCuote,FlagOpenCuote) =>{
    if (FlagOpenCuote){
      setScaleCard('scale(0)');
      setScaleCuote('scale(1)');
      setZindex(4444);
      setFlagOpenCuote(false);
    }  
    else{
      setScaleCard('scale(1)');
      setScaleCuote('scale(0)');
      setZindex(-3333);
      setFlagOpenCuote(true);
    }

  }

  //Estas funciones corresponden a cada card y lo que hacen es llamar a la funcion openclosecuote. Estas funciones son las que ejecuta el evento click en el boton de la card coche y el boton del icono de volver atras de la card cuota

  const ClickOpenCloseCuote1 = () => {
    OpenCloseCuote(setScaleCard1,setScaleCuote1,setZindex1,setFlagOpenCuote1,FlagOpenCuote1);
  }
  
  const ClickOpenCloseCuote2 = () => {
    OpenCloseCuote(setScaleCard2,setScaleCuote2,setZindex2,setFlagOpenCuote2,FlagOpenCuote2);
  }
  
  const ClickOpenCloseCuote3 = () => {
    OpenCloseCuote(setScaleCard3,setScaleCuote3,setZindex3,setFlagOpenCuote3,FlagOpenCuote3);
  }

  const ClickOpenCloseCuote4 = () => {
    OpenCloseCuote(setScaleCard4,setScaleCuote4,setZindex4,setFlagOpenCuote4,FlagOpenCuote4);
  }

  const ClickOpenCloseCuote5 = () => {
    OpenCloseCuote(setScaleCard5,setScaleCuote5,setZindex5,setFlagOpenCuote5,FlagOpenCuote5);
  }

  const ClickOpenCloseCuote6 = () => {
    OpenCloseCuote(setScaleCard6,setScaleCuote6,setZindex6,setFlagOpenCuote6,FlagOpenCuote6);
  }

  const ClickOpenCloseCuote7 = () => {
    OpenCloseCuote(setScaleCard7,setScaleCuote7,setZindex7,setFlagOpenCuote7,FlagOpenCuote7);
  }

  const ClickOpenCloseCuote8 = () => {
    OpenCloseCuote(setScaleCard8,setScaleCuote8,setZindex8,setFlagOpenCuote8,FlagOpenCuote8);
  }

  const ClickOpenCloseCuote9 = () => {
    OpenCloseCuote(setScaleCard9,setScaleCuote9,setZindex9,setFlagOpenCuote9,FlagOpenCuote9);
  }

  const ClickOpenCloseCuote10 = () => {
    OpenCloseCuote(setScaleCard10,setScaleCuote10,setZindex10,setFlagOpenCuote10,FlagOpenCuote10);
  }
  
  const ClickOpenCloseCuote11 = () => {
    OpenCloseCuote(setScaleCard11,setScaleCuote11,setZindex11,setFlagOpenCuote11,FlagOpenCuote11);
  }

  const ClickOpenCloseCuote12 = () => {
    OpenCloseCuote(setScaleCard12,setScaleCuote12,setZindex12,setFlagOpenCuote12,FlagOpenCuote12);
  }

  const ClickOpenCloseCuote13 = () => {
    OpenCloseCuote(setScaleCard13,setScaleCuote13,setZindex13,setFlagOpenCuote13,FlagOpenCuote13);
  }

  const ClickOpenCloseCuote14 = () => {
    OpenCloseCuote(setScaleCard14,setScaleCuote14,setZindex14,setFlagOpenCuote14,FlagOpenCuote14);
  }

  const ClickOpenCloseCuote15 = () => {
    OpenCloseCuote(setScaleCard15,setScaleCuote15,setZindex15,setFlagOpenCuote15,FlagOpenCuote15);
  }



// ESTO ME CIERRA LAS CARDCUOTE QUE ESTEN ABIERTAS CUANDO HAGO CLICK EN UNA OPCION DEL SELECT  //


const FlagOpenCuotes = [FlagOpenCuote1,FlagOpenCuote2,FlagOpenCuote3,FlagOpenCuote4,FlagOpenCuote5,FlagOpenCuote6,FlagOpenCuote7,FlagOpenCuote8,FlagOpenCuote9,FlagOpenCuote10,FlagOpenCuote11,FlagOpenCuote12,FlagOpenCuote13,FlagOpenCuote14,FlagOpenCuote15];

const ClickOpenCloseCuotes = [ClickOpenCloseCuote1,ClickOpenCloseCuote2,ClickOpenCloseCuote3,ClickOpenCloseCuote4,ClickOpenCloseCuote5,ClickOpenCloseCuote6,ClickOpenCloseCuote7,ClickOpenCloseCuote8,ClickOpenCloseCuote9,ClickOpenCloseCuote10,ClickOpenCloseCuote11,ClickOpenCloseCuote12,ClickOpenCloseCuote13,ClickOpenCloseCuote14,ClickOpenCloseCuote15];

  // Este useffect se me ejecuta cuando se hace click en alguna opcion de los select y me cierra todas las card cuota abiertas. Utiliza todas las flags que que me indican que una card cuota esta abierta y cuando detecta la que esta abierta por medio de un for me ejecuta la funcion para cerrarla

  useEffect(() => {
    for (let i=0; i<FlagOpenCuotes.length; i++)
    if (FlagOpenCuotes[i]==false){ 
     ClickOpenCloseCuotes[i]()
    }   
  }, [ActivEffect])

  

  // Estos son los datos correspondientes al mapeo de las cardcoche

  const CardDatos = [
    {
      id:1,
      imagen:'imagenes/descarga.jpg',
      tipo:'Auto', 
      nombre:'Onyx Joy',
      precio:'$ 789.000',
      visibility: display1,
      claseEfecto: efectoCard1,
      scale:ScaleCard1,
      ClickBoton:ClickOpenCloseCuote1,
      
    },
    {
      id:2,
      imagen:'imagenes/prisma.jpg',
      tipo:'Auto',
      nombre:'Prisma',
      precio:'$ 855.000',
      visibility: display2,
      claseEfecto: efectoCard2,
      scale:ScaleCard2,
      ClickBoton:ClickOpenCloseCuote2,
    },
    {
      id:3,
      imagen:'imagenes/prisma joy.jpg',
      tipo:'Auto',
      nombre:'Prisma Joy',
      precio:'$ 875.000',
      visibility: display3,
      claseEfecto: efectoCard3,
      scale:ScaleCard3,
      ClickBoton:ClickOpenCloseCuote3,
    },
    {
      id:4,
      imagen:'imagenes/cruze.jpg',
      tipo:'Auto',
      nombre:'Nuevo Cruze',
      precio:'$ 1.604.900',
      visibility: display4,
      claseEfecto: efectoCard4,
      scale:ScaleCard4,
      ClickBoton:ClickOpenCloseCuote4,
    },
    {
      id:5,
      imagen:'imagenes/cruze heat.jpg',
      tipo:'Auto',
      nombre:'Nuevo Cruze 5',
      precio:'2.100.000',
      visibility: display5,
      claseEfecto: efectoCard5,
      scale:ScaleCard5,
      ClickBoton:ClickOpenCloseCuote5,
    },
    {
      id:6,
      imagen:'imagenes/onix.jpg',
      tipo:'Auto',
      nombre:'Onyx Beta',
      precio:'$1.300.000',
      visibility: display6,
      claseEfecto: efectoCard6,
      scale:ScaleCard6,
      ClickBoton:ClickOpenCloseCuote6,
    },
    {
      id:7,
      imagen:'imagenes/onix con baul.jpg',
      tipo:'Auto',
      nombre:'Onyx Baul',
      precio:'$1.200.000',
      visibility: display7,
      claseEfecto: efectoCard7,
      scale:ScaleCard7,
      ClickBoton:ClickOpenCloseCuote7,
    },
    {
      id:8,
      imagen:'imagenes/camaro.jpg',
      tipo:'Auto',
      nombre:'Camaro',
      precio:'$ 2.900.000',
      visibility: display8,
      claseEfecto: efectoCard8,
      scale:ScaleCard8,
      ClickBoton:ClickOpenCloseCuote8,
    },
    {
      id:9,
      imagen:'imagenes/spin.jpg',
      tipo:'Monovolumen',
      nombre:'Spin',
      precio:'$1.456.000',
      visibility: display9,
      claseEfecto: efectoCard9,
      scale:ScaleCard9,
      ClickBoton:ClickOpenCloseCuote9,
    },
    {
      id:10,
      imagen:'imagenes/spin activ.jpg',
      tipo:'Monovolumen',
      nombre:'Spin Active',
      precio: '$ 1.850.000',
      visibility: display10,
      claseEfecto: efectoCard10,
      scale:ScaleCard10,
      ClickBoton:ClickOpenCloseCuote10,
    },
    {
      id:11,
      imagen:'imagenes/s10 doble.jpg',
      tipo:'Pick Ups',
      nombre:'S10 Cabina Doble',
      precio:'$ 2.199.000',
      visibility: display11,
      claseEfecto: efectoCard11,
      scale:ScaleCard11,
      ClickBoton:ClickOpenCloseCuote11,
    },
    {
      id:12,
      imagen:'imagenes/s10 simple.jpg',
      tipo:'Pick Ups',
      nombre:'S10 Cabina Simple',
      precio:'$ 1.950.000',
      visibility: display12,
      claseEfecto: efectoCard12,
      scale:ScaleCard12,
      ClickBoton:ClickOpenCloseCuote12,
    },
    {
      id:13,
      imagen:'imagenes/tracker.jpg',
      tipo:'Suv',
      nombre:'Tracker',
      precio:'$ 2.400.500',
      visibility: display13,
      claseEfecto: efectoCard13,
      scale:ScaleCard13,
      ClickBoton:ClickOpenCloseCuote13,
    },
    {
      id:14,
      imagen: 'imagenes/equinox.jpg',
      tipo:'Suv',
      nombre:'Equinox',
      precio:'$ 3.400.000',
      visibility: display14,
      claseEfecto: efectoCard14,
      scale:ScaleCard14,
      ClickBoton:ClickOpenCloseCuote14,
    },
    {
      id:15,
      imagen:'imagenes/trabi.jpg',
      tipo:'Suv',
      nombre:'Trailblazer',
      precio:'$ 3.500.000',
      visibility: display15,
      claseEfecto: efectoCard15,
      scale:ScaleCard15,
      ClickBoton:ClickOpenCloseCuote15,
    },
  ]


// Estos son los datos correspondientes al mapeo de las cardcoche

  const CardDateCoute = [
    {
      id:1,
      visibility:display1,
      scalee:ScaleCuote1,
      zindex:Zindex1,
      ClickBotonn:ClickOpenCloseCuote1, 
    },
    {
      id:2,
      visibility: display2,
      scalee:ScaleCuote2,
      zindex:Zindex2,
      ClickBotonn:ClickOpenCloseCuote2,
    },
    {
      id:3,
      visibility: display3,
      scalee:ScaleCuote3,
      zindex:Zindex3,
      ClickBotonn:ClickOpenCloseCuote3,
    },
    {
      id:4,
      visibility: display4,
      scalee:ScaleCuote4,
      zindex:Zindex4,
      ClickBotonn:ClickOpenCloseCuote4,
    },
    {
      id:5,
      visibility: display5,
      scalee:ScaleCuote5,
      zindex:Zindex5,
      ClickBotonn:ClickOpenCloseCuote5,
    },
    {
      id:6,
      visibility: display6,
      scalee:ScaleCuote6,
      zindex:Zindex6,
      ClickBotonn:ClickOpenCloseCuote6,
    },
    {
      id:7,
      visibility: display7,
      scalee:ScaleCuote7,
      zindex:Zindex7,
      ClickBotonn:ClickOpenCloseCuote7,
    },
    {
      id:8,
      visibility: display8,
      scalee:ScaleCuote8,
      zindex:Zindex8,
      ClickBotonn:ClickOpenCloseCuote8,
    },
    {
      id:9,
      visibility: display9,
      scalee:ScaleCuote9,
      zindex:Zindex9,
      ClickBotonn:ClickOpenCloseCuote9
    },
    {
      id:10,
      visibility: display10,
      scalee:ScaleCuote10,
      zindex:Zindex10,
      ClickBotonn:ClickOpenCloseCuote10
    },
    {
      id:11,
      visibility: display11,
      scalee:ScaleCuote11,
      zindex:Zindex11,
      ClickBotonn:ClickOpenCloseCuote11
    },
    {
      id:12,
      visibility: display12,
      scalee:ScaleCuote12,
      zindex:Zindex12,
      ClickBotonn:ClickOpenCloseCuote12
    },
    {
      id:13,
      visibility: display13,
      scalee:ScaleCuote13,
      zindex:Zindex13,
      ClickBotonn:ClickOpenCloseCuote13
    },
    {
      id:14,
      visibility: display14,
      scalee:ScaleCuote14,
      zindex:Zindex14,
      ClickBotonn:ClickOpenCloseCuote14
    },
    {
      id:15,
      visibility: display15,
      scalee:ScaleCuote15,
      zindex:Zindex15,
      ClickBotonn:ClickOpenCloseCuote15
    },
  ]


//solucion poner las posiciones para que concidan las card de forma manual y a los que son 2 como suv agregarle uno mas asi quedan 3 y no me falla y lo otro es trataer de poner a las card cuota en un contenedor aparte asi interactua por su lado 
  
  return (
    <>
      <div className="container-cards flexbox" style={  {left:propMoveContainer}}>
        { 
        CardDatos.map(({ id, imagen, tipo, nombre, precio, visibility, claseEfecto, scale, ClickBoton}) => <CardCoche key={id} imagen={imagen} tipo={tipo} nombre={nombre} precio={precio} visibility={visibility} claseEfecto={claseEfecto} scale={scale} ClickBoton={ClickBoton} />)
        }
      </div>  
      <div className="container-cards-coute flexbox" style={  {left:propMoveContainer,right:propMoveContainer2}}>  
        { 
        CardDateCoute.map(({ id, visibility, margin, scalee, zindex, ClickBotonn, flagBtnCuotee}) => <CardCuota key={id} visibility={visibility} margin={margin} scalee={scalee} zindex={zindex} ClickBotonn={ClickBotonn} FlagOpenCuotes={FlagOpenCuotes}/>)
        }       

      </div>
    </>    
  );
}

export default CardGrupal;

/*
const [flagBtnCuotee1, setflagBtnCuotee1] = useState(false)

 const ActiveFlagCuote1 = () =>{
  setflagBtnCuotee1(true)
 }

  const CardDatos = [
    {
      id:1,
      imagen:'imagenes/descarga.jpg',
      tipo:'Auto', 
      nombre:'Onyx Joy',
      precio:'$ 789.000',
      visibility: display1,
      claseEfecto: efectoCard1,
      scale:ScaleCard1,
      ClickBoton:ClickOpenCloseCuote1,
      ActiveFlagCuote:ActiveFlagCuote1,
    },
    {
      id:2,
      imagen:'imagenes/prisma.jpg',
      tipo:'Auto',
      nombre:'Prisma',
      precio:'$ 855.000',
      visibility: display2,
      claseEfecto: efectoCard2,
      scale:ScaleCard2,
      ClickBoton:ClickOpenCloseCuote2,
    },
    {
      id:3,
      imagen:'imagenes/prisma joy.jpg',
      tipo:'Auto',
      nombre:'Prisma Joy',
      precio:'$ 875.000',
      visibility: display3,
      claseEfecto: efectoCard3,
      scale:ScaleCard3,
      ClickBoton:ClickOpenCloseCuote3,
    },
    {
      id:4,
      imagen:'imagenes/cruze.jpg',
      tipo:'Auto',
      nombre:'Nuevo Cruze',
      precio:'$ 1.604.900',
      visibility: display4,
      claseEfecto: efectoCard4,
      scale:ScaleCard4,
      ClickBoton:ClickOpenCloseCuote4,
    },
    {
      id:5,
      imagen:'imagenes/cruze heat.jpg',
      tipo:'Auto',
      nombre:'Nuevo Cruze 5',
      precio:'2.100.000',
      visibility: display5,
      claseEfecto: efectoCard5,
      scale:ScaleCard5,
      ClickBoton:ClickOpenCloseCuote5,
    },
    {
      id:6,
      imagen:'imagenes/onix.jpg',
      tipo:'Auto',
      nombre:'Onyx Beta',
      precio:'$1.300.000',
      visibility: display6,
      claseEfecto: efectoCard6,
      scale:ScaleCard6,
      ClickBoton:ClickOpenCloseCuote6,
    },
    {
      id:7,
      imagen:'imagenes/onix con baul.jpg',
      tipo:'Auto',
      nombre:'Onyx Baul',
      precio:'$1.200.000',
      visibility: display7,
      claseEfecto: efectoCard7,
      scale:ScaleCard7,
      ClickBoton:ClickOpenCloseCuote7,
    },
    {
      id:8,
      imagen:'imagenes/camaro.jpg',
      tipo:'Auto',
      nombre:'Camaro',
      precio:'$ 2.900.000',
      visibility: display8,
      claseEfecto: efectoCard8,
      scale:ScaleCard8,
      ClickBoton:ClickOpenCloseCuote8,
    },
    {
      id:9,
      imagen:'imagenes/spin.jpg',
      tipo:'Monovolumen',
      nombre:'Spin',
      precio:'$1.456.000',
      visibility: display9,
      claseEfecto: efectoCard9,
      scale:ScaleCard9,
      ClickBoton:ClickOpenCloseCuote9,
    },
    {
      id:10,
      imagen:'imagenes/spin activ.jpg',
      tipo:'Monovolumen',
      nombre:'Spin Active',
      precio: '$ 1.850.000',
      visibility: display10,
      claseEfecto: efectoCard10,
      scale:ScaleCard10,
      ClickBoton:ClickOpenCloseCuote10,
    },
    {
      id:11,
      imagen:'imagenes/s10 doble.jpg',
      tipo:'Pick Ups',
      nombre:'S10 Cabina Doble',
      precio:'$ 2.199.000',
      visibility: display11,
      claseEfecto: efectoCard11,
      scale:ScaleCard11,
      ClickBoton:ClickOpenCloseCuote11,
    },
    {
      id:12,
      imagen:'imagenes/s10 simple.jpg',
      tipo:'Pick Ups',
      nombre:'S10 Cabina Simple',
      precio:'$ 1.950.000',
      visibility: display12,
      claseEfecto: efectoCard12,
      scale:ScaleCard12,
      ClickBoton:ClickOpenCloseCuote12,
    },
    {
      id:13,
      imagen:'imagenes/tracker.jpg',
      tipo:'Suv',
      nombre:'Tracker',
      precio:'$ 2.400.500',
      visibility: display13,
      claseEfecto: efectoCard13,
      scale:ScaleCard13,
      ClickBoton:ClickOpenCloseCuote13,
    },
    {
      id:14,
      imagen: 'imagenes/equinox.jpg',
      tipo:'Suv',
      nombre:'Equinox',
      precio:'$ 3.400.000',
      visibility: display14,
      claseEfecto: efectoCard14,
      scale:ScaleCard14,
      ClickBoton:ClickOpenCloseCuote14,
    },
    {
      id:15,
      imagen:'imagenes/trabi.jpg',
      tipo:'Suv',
      nombre:'Trailblazer',
      precio:'$ 3.500.000',
      visibility: display15,
      claseEfecto: efectoCard15,
      scale:ScaleCard15,
      ClickBoton:ClickOpenCloseCuote15,
    },
    
  ]

  const CardDateCoute = [
    {
      id:1,
      visibility:display1,
      scalee:ScaleCuote1,
      zindex:Zindex1,
      ClickBotonn:ClickOpenCloseCuote1, 
      
      flagBtnCuotee:flagBtnCuotee1,
    },
    {
      id:2,
      visibility: display2,
      scalee:ScaleCuote2,
      zindex:Zindex2,
      ClickBotonn:ClickOpenCloseCuote2,
    },
    {
      id:3,
      visibility: display3,
      scalee:ScaleCuote3,
      zindex:Zindex3,
      ClickBotonn:ClickOpenCloseCuote3,
      
    },
    {
      id:4,
      visibility: display4,
      scalee:ScaleCuote4,
      zindex:Zindex4,
      ClickBotonn:ClickOpenCloseCuote4,
      
    },
    {
      id:5,
      visibility: display5,
      scalee:ScaleCuote5,
      zindex:Zindex5,
      ClickBotonn:ClickOpenCloseCuote5,
    },
    {
      id:6,
      visibility: display6,
      scalee:ScaleCuote6,
      zindex:Zindex6,
      ClickBotonn:ClickOpenCloseCuote6,
    },
    {
      id:7,
      visibility: display7,
      scalee:ScaleCuote7,
      zindex:Zindex7,
      ClickBotonn:ClickOpenCloseCuote7,
    },
    {
      id:8,
      visibility: display8,
      scalee:ScaleCuote8,
      zindex:Zindex8,
      ClickBotonn:ClickOpenCloseCuote8,
    },
    {
      id:9,
      visibility: display9,
      scalee:ScaleCuote9,
      zindex:Zindex9,
      ClickBotonn:ClickOpenCloseCuote9
    },
    {
      id:10,
      visibility: display10,
      scalee:ScaleCuote10,
      zindex:Zindex10,
      ClickBotonn:ClickOpenCloseCuote10
    },
    {
      id:11,
      visibility: display11,
      scalee:ScaleCuote11,
      zindex:Zindex11,
      ClickBotonn:ClickOpenCloseCuote11
    },
    {
      id:12,
      visibility: display12,
      scalee:ScaleCuote12,
      zindex:Zindex12,
      ClickBotonn:ClickOpenCloseCuote12
    },
    {
      id:13,
      visibility: display13,
      scalee:ScaleCuote13,
      zindex:Zindex13,
      ClickBotonn:ClickOpenCloseCuote13
    },
    {
      id:14,
      visibility: display14,
      scalee:ScaleCuote14,
      zindex:Zindex14,
      ClickBotonn:ClickOpenCloseCuote14
    },
    {
      id:15,
      visibility: display15,
      scalee:ScaleCuote15,
      zindex:Zindex15,
      ClickBotonn:ClickOpenCloseCuote15
    },
  ]

//solucion poner las posiciones para que concidan las card de forma manual y a los que son 2 como suv agregarle uno mas asi quedan 3 y no me falla y lo otro es trataer de poner a las card cuota en un contenedor aparte asi interactua por su lado 
  
  return (
    <>
      <div className="container-cards flexbox" style={  {left:propMoveContainer}}>
        { 
        CardDatos.map(({ id, imagen, tipo, nombre, precio, visibility, claseEfecto, scale, ClickBoton, ActiveFlagCuote }) => <CardCoche key={id} imagen={imagen} tipo={tipo} nombre={nombre} precio={precio} visibility={visibility} claseEfecto={claseEfecto} scale={scale} ClickBoton={ClickBoton} ActiveFlagCuote={ActiveFlagCuote}/>)
        }
      </div>  
      <div className="container-cards-coute flexbox" style={  {left:propMoveContainer,right:propMoveContainer2}}>  
        { 
        CardDateCoute.map(({ id, visibility, margin, scalee, zindex, ClickBotonn, flagBtnCuotee}) => <CardCuota key={id} visibility={visibility} margin={margin} scalee={scalee} zindex={zindex} ClickBotonn={ClickBotonn} flagBtnCuotee={flagBtnCuotee}/>)
        }       

      </div>
    </>    
  );
}

export default CardGrupal;

*/