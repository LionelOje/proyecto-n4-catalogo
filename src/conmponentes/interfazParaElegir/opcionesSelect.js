import React, {useContext} from 'react';
import { ContextTheme } from '../../App';
import './opcionesSelect.css'


/// ESTE COMPONENTE ME CONTIENE LAS OPCIONES QUE SE DESPLIEGAN CUANDO HAGO CLICK EN ALGUN SELECT, Y LO UTILIZO EN UN MAP DENTRO DEL COMPONENTE InterfazParaElegir

const OpcionsSelect = ({CloseOpcions,opacity,margenAbajo,transform,opcion1,opcion2,opcion3,opcion4,opcion5,funcionn1,funcionn2,funcionn3,funcionn4,funcionn5, cla, positionOpcions, margenizq  }) => {
    
    const {ColorThemeWhite, ColorThemeGrey, NightBoxShadow} = useContext(ContextTheme);

    return (
        <div className={positionOpcions}>
        <div className={cla} onClick={()=>{CloseOpcions()}} style={{marginBottom:margenAbajo, marginLeft:margenizq, opacity:opacity, transform:transform, color:ColorThemeWhite, background:ColorThemeGrey}}>
            <p className="categorias" onClick={()=> {funcionn1()} }>{opcion1}</p>
            <p className="categorias" onClick={()=> {funcionn2()} }>{opcion2}</p>
            <p className="categorias" onClick={()=> {funcionn3()} }>{opcion3}</p>
            <p className="categorias" onClick={()=> {funcionn4()} }>{opcion4}</p>
            <p className="categorias" onClick={()=> {funcionn5()} }>{opcion5}</p>
        </div>
        </div>

    );
  }
  
  export default OpcionsSelect;
  