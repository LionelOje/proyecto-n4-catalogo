import React, {useContext} from 'react';
import './selectNombre.css'
import './iconSelect.css'
import { ContextTheme } from '../../App';


/// ESTE ES EL COMPONENTE DEL SELECT Y LO UTILIZO DENTRO DE UN MAP EN EL COMPONENTE InterfazParaEleir ///

const SelectNombre = ({NameOpcion, FuncionEstado, rotate, positionSelects}) => {
   
    const {ColorThemeWhite, ColorThemeGrey, NightBoxShadow} = useContext(ContextTheme);

    return (
            <div className={positionSelects}>                             
            <div className="select" style={{background:ColorThemeGrey, color:ColorThemeWhite}}
            onClick={()=> {FuncionEstado()} } >
                <p className="selector" >{NameOpcion}</p>
                <span className="simbolo icon-play3" style={{transform:rotate}}></span>
            </div>
            </div>
    );
  }
  
 export default SelectNombre;

 