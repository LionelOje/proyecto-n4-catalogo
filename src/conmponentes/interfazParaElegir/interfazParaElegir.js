import React, {useState, useContext} from 'react';
import './interfazParaElegir.css'
import SelectNombre from './selectNombre'
import OpcionsSelect from './opcionesSelect';
import { ContextTheme } from '../../App';


   // ESTE COMPONENTE CONTIENE LA PARTE DONDE SE SITUAN LOS SELECTS, PARA PODER FILTRAR LA LISTA DE AUTOS SEGUN DISTINTAS CATEGORIAS

   const InterfazParaElegir = ({flagOpInterfaz,DisplayOpcion1,DisplayOpcion2,DisplayOpcion3,DisplayOpcion4,DisplayOpcion5,DisplayOpcion6,DisplayOpcion7,DisplayOpcion8,DisplayOpcion9,DisplayOpcion10,DisplayOpcion11,DisplayOpcion12,NameSelect1,NameSelect2,NameSelect3}) => {

    const {ImgInterfazTheme} = useContext(ContextTheme);
  

    ///FUNCIONALIDAD PARA HACER ROTAR LOS ICONOS DEL SELECT///
    
    //Estados que corresponden a cada icono de cada select y que me cambia el estilo css que me hace girar el icono en 90 grados cuando se hace click en el select y se despliegan las opciones
    const [RotateIcon1,setRotateIcon1] = useState(null);
    const [RotateIcon2,setRotateIcon2] = useState(null);
    const [RotateIcon3,setRotateIcon3] = useState(null);

    //Flags para controlar si el icono gira en direccion open o close
    const[FlagRotateIcon1,setFlagRotateIcon1] = useState(false);
    const[FlagRotateIcon2,setFlagRotateIcon2] = useState(false);
    const[FlagRotateIcon3,setFlagRotateIcon3] = useState(false);

    //Variables para manejar si quiero que la funcion de abajo me haga girar el  icono en direccion open o close
    let a = 1;
    let b = 2;
    let c = 1;


// Esta funcion lo que hace es, que si el flag del icono del select esta en falso, y le estoy pasando la variable que uso para poner el icono en modo open, me pone el icono en posicion open, y sino, me lo pone en posicion close. En ves de usar una funcion que sea para poner el icono en posicion close, uso una variable que me ejecuta el true o el false del condicional.

const RotateIconSelect = (variable, setRotateIcon, setFlagRotateIcon, FlagRotateIcon) => {
  if(!FlagRotateIcon && c==variable){
    setRotateIcon('rotate(270deg)'); 
    setFlagRotateIcon(true);
  }
  else{
    setRotateIcon('rotate(90deg)');
    setFlagRotateIcon(false);
  }
 }



    ///ESTO ES PARA CUANDO HAGA CLICK EN SELECT SE ME ABRAN LAS OPCIONES Y PARA QUE CUANDO HAGA CLICK DEVUELTA SE ME CIERREN///


    // Estos estados corresponden a cada select, y como primer valor tienen esa clase que me tiene a las opciones del select en display none

    const [clasOp1,setclasOp1] = useState('contenedor-categoriasNone');
    const [clasOp2,setclasOp2] = useState('contenedor-categoriasNone');
    const [clasOp3,setclasOp3] = useState('contenedor-categoriasNone');
    

    // Estos estados corresponden a cada select, y son para controlar que select esta abierto y cual no

    const[FlagOpenOp1,setFlagOpenOp1] = useState(false);
    const[FlagOpenOp2,setFlagOpenOp2] = useState(false);
    const[FlagOpenOp3,setFlagOpenOp3] = useState(false);


   // Esta funcion con temporizador, me cambia la clase del select que le indique por parametro, a modo display none luego de un segundo

    const ChangeClass = (set) =>{ 
      return setTimeout(function(){ set('contenedor-categoriasNone'); }, 1000);
    }


   //Esta funcion me abre las opciones del select. Me cambia las clases de las opciones de los select a modo display block, ejecutando un efecto keyfram de aparecer

   const OpenOp = (setclas, setFlag) => {
    setclas('contenedor-categoriass');
    setFlag(true);
   }

  //Esta funcion me cierra las opciones del select. Me cambia la misma clase que en el open, pero ejecutando un efecto keyfram de desaparecer. A lo ultimo me llama a la funcion Change... que me pone la clase de las opciones de los select a modo display none, y queda como estaba al principio. Lo tuve que hacer asi, por que no podia poner en esta misma funcion un keyfram de desaparecer y la ves un display none, por que se me ejecutaba primero el none y no se veia el efecto

   const CloseOp = (setclas, setFlag) => {
    setclas('contenedor-categorias');
    setFlag(false);
    ChangeClass(setclas);
   }


// CUANDO ES TAMAÑO MOBIL, EL MONOVOLUMEN COMO NOMBRE DE OPCION NO ME ENTRA, POR ESO CON ESTO DETECTO CUANDO ES MOBIL Y SI ES MOBIL LO ACORTO ASI ENTRA EN EL CONTENEDOR DE LAS OPCIONES DEL SELECT

const[NameMonovolumenMobil, setNameMovolumenMobil] = useState(false);


const NameOpcionMonovolumenMobil = () => {

  let windowSize = window.innerWidth;

  (windowSize<370) ? setNameMovolumenMobil('Monovo...') : setNameMovolumenMobil('Monovolumen');
 }




  // Cada una de estas funciones corresponden al evento click de cada select para que se me abren o cierren las opciones y tambien me rote el icono del select. Para ello llamo a 3 funciones, una para rotar el icono otra para abrir las opciones y en caso de que la bandera este en true osea abierta me las cierra

    const OpenOpcions = () => {
     
      RotateIconSelect(a, setRotateIcon1, setFlagRotateIcon1, FlagRotateIcon1);

      (!FlagOpenOp1) ?  OpenOp(setclasOp1, setFlagOpenOp1)  : CloseOp(setclasOp1, setFlagOpenOp1);
    }

  const OpenOpcions2 = () => {

    RotateIconSelect(a, setRotateIcon2, setFlagRotateIcon2, FlagRotateIcon2);
    
    if (!FlagOpenOp2){
      OpenOp(setclasOp2, setFlagOpenOp2);
      NameOpcionMonovolumenMobil();
    }
    else{
      CloseOp(setclasOp2, setFlagOpenOp2);
    }
  }

  const OpenOpcions3 = () => {

    RotateIconSelect(a, setRotateIcon3, setFlagRotateIcon3, FlagRotateIcon3);

    (!FlagOpenOp3) ?  OpenOp(setclasOp3, setFlagOpenOp3)  : CloseOp(setclasOp3, setFlagOpenOp3);    
  }



/// ESTO ES PARA QUE CUANDO HAGO CLICK EN ALGUNA OPCION ME LA CIERRE SI ESTA ABIERTA, NO SOLO ESA SINO LAS OPCIONES DE LOS 3 SELECT POR LAS DUDAS DE QUE TENGA OTRO ABIERTO. ME LO CIERRA SOLO SI EL FLAG QUE SE PONE EN TRUE CUANDO ABRO UNA OPCION ESTA EN TRUE PRECISAMENTE, TAMBIEN ME GIRA LOS ICONOS DEL SELECT EN MODO CLOSE SI ESTAN EN MODO OPEN ///


  const CloseOpcions = () => {

    RotateIconSelect(b, setRotateIcon1, setFlagRotateIcon1, FlagRotateIcon1);
    RotateIconSelect(b, setRotateIcon2, setFlagRotateIcon2, FlagRotateIcon2);
    RotateIconSelect(b, setRotateIcon3, setFlagRotateIcon3, FlagRotateIcon3);
    
    if (FlagOpenOp1){
      CloseOp(setclasOp1, setFlagOpenOp1);
    }

    if(FlagOpenOp2){
      CloseOp(setclasOp2, setFlagOpenOp2);
    }

    if(FlagOpenOp3){
      CloseOp(setclasOp3, setFlagOpenOp3);
    }
  }




//////// ESTO ES PARA QUE CUANDO HAGA EL MOUSE PARA ABAJO APARESCA LA INTERFAZ ACTIV ES PARA EL EVENTO ONMOUSEOVER Y DESACTIV PARA EL MAUSELEAVE//////////

    const [Bandera,setBandera] = useState(false);

    // Estado para el estilo top de la interfaz, haciendo que se desplaze hacia arriba o abajo

    const [TopInterfaz,setTopInterfaz] = useState(null);
    
  
    //Funcion del  evento hover de cuando paso el mouse por el div que uso como activador de la interfaz, me activa el menu solo si esta en tamaño mayor a mobil, por que si es mobil no uso efecto hover

    const ActivInterfaz = () => {

      let windowSize = window.innerWidth;

      (windowSize<700) ? setTopInterfaz(null) : setTopInterfaz('0px');
    }
  
    
    // Funcion del evento mouse leave que tambien se activa a partir del div que uso como activador en la interfaz. Si ninguna de las opciones de los select estan abiertas,y si no esta elejida una opcion con pocas card (flagOpInterfaz[0]==false) que me tenga que dejar la intefaz activada siempre, desplazo la interfaz para abajo. 
    
    const DesactivInterfaz = () =>{
   
      if (FlagOpenOp1==false && FlagOpenOp2==false && FlagOpenOp3==false){ 
      if (Bandera==false && flagOpInterfaz[0]==false){ 
       setTopInterfaz('-140px');
      }       
    }
  }



///////////////////// EL SCROLL QUE HACE APARECER LA INTERFAZ DE LOS SELECT /////////////
    
    // Funcion para activar interfaz de los select con scroll //

    const ActivInterScroll = () => {
      
      // Si ninguna de los 3 select estan abiertos, guardo en la variable scrollTop la medida actual del scroll del cursor, y guardo en un array las 3 medidas en las que voy a activar el scroll. Cada medida corresponde a la cantidad de cards que se muestren en pantalla y esa cantidad lo determina que opcion este elejida en el momento. El flag lo utilizo para saber si tengo que ejecutar el scroll con la medida en la que se ven todas las cards, false es si y true es no, osea que se eligio una de las opciones de los select. Si lguno de los select esta abierto, no se ejecuta el scroll y el interfaz queda en modo activo

      if(!FlagOpenOp1 && !FlagOpenOp2 && !FlagOpenOp3){
      
      let scrollTop = document.documentElement.scrollTop; 
      let flag = false;
      let medidas = [-444,200,400];
     
     // Funciones para el scroll de la FlagOpInterfaz[0] = true, osea cuando solo hay dos cards como vista. Aca el evento scroll esta como desactivado, siempre se me muestra la interfaz de los seletc

      const ScrollInterfazTrueB = () => {
        setTopInterfaz('0');
      }
      const ScrollInterfazFalseB = () => {
        setTopInterfaz('-140px');

     // Funciones para cuando el scroll de las otras flagOpInterfaz esten en true, osea cuando hay mas de dos cards como vista por lo que tengo que bjar para ver las otras y nesesito que el evento scroll este activo

     }  
      const ScrollInterfazTrue = () => {
        setTopInterfaz('0');
        setBandera(true);
     } 
  
     const ScrollInterfazFalse = () => {
       setTopInterfaz('-140px');
       setBandera(false);
     } 
 

     // Este for detecta por medio del flagOpInterfaz en que medida se deve activar y desactivar el interfaz. Recordar que el flagOpInterfaz era true, segun en que opcion del select se elija, y que dependia de la cantidad de cards que se mostraban. Si esta en true la flagOpInterfaz[0]. le corresponde una medida que hace que el scroll no se active ni desactive, la interfaz en esta medida siempre esta abierta por que es la opcion del select en la que solo hay dos cards, esas funciones que puse en este camino de la app es como que no hacen nada en si. Ahora si estan activadas cualquiera de las otrs dos flagOpInterfaz, Utilizo funciones que me activen y desctiven el scroll y me pongan en true el estado bandera que declare en la parte del hover de la interfaz mas arriba. Eso me permite que no se active el mauseleave de la interfaz cuando el scroll de estas opciones de los select esten abiertas

     const FuncionAuxiliarSroll = (medida, truee, falsee) => {
       (medida < scrollTop) ? truee() : falsee();
     }

     for (let i=0; i<flagOpInterfaz.length; i++){ 
       if(flagOpInterfaz[i]) {       
          if (i==0){
            flag = true;
            FuncionAuxiliarSroll(medidas[i], ScrollInterfazTrueB, ScrollInterfazFalseB);
          }
          else{
            flag = true
            FuncionAuxiliarSroll(medidas[i], ScrollInterfazTrue, ScrollInterfazFalse);
         }          
       }
     } 

      // Si no elegi ninguna opcion de los select, la pantalla me queda en tamaño normal que es cuando estan todas las cards, eso me lo dice el flag false. Si esta en false, osea que se ven todas las cards uso como medida 1000 y ejecuto el scroll segun corresponda

      if (!flag){
        
       if (!FlagOpenOp1){  
        FuncionAuxiliarSroll(900, ScrollInterfazTrue, ScrollInterfazFalse);
       }
      }
    }
  }      
       
  //Me llama la funcion que me activa el evento scroll     
  window.onscroll = () => {
    ActivInterScroll();
  }
    


  // ESTE ES EL OBJETO QUE CONTIENE LOS DATOS DE LAS OPCIONES QUE LUEGO ITERO CON EL METODO MAP ABAJO.

   const DatosOpcions = [
      {
        id:1,
        opcion1:'Motor 1.6', 
        opcion2:'Motor 1.8',
        opcion3:'Motor 2.0',
        funcionn1: DisplayOpcion1,
        funcionn2: DisplayOpcion2,
        funcionn3: DisplayOpcion3,
        margenAbajo:"-83px",
        CloseOpcions: CloseOpcions,
        cla: clasOp1,
        positionOpcions: 'positionOpcions1',
      },
      {
         id:2,
         opcion1:'Auto', 
         opcion2: NameMonovolumenMobil,
         opcion3:'Suv',
         opcion4:'Pickup',
         funcionn1: DisplayOpcion4,
         funcionn2: DisplayOpcion5,
         funcionn3: DisplayOpcion6,
         funcionn4: DisplayOpcion7,
         margenAbajo:'-41px',
         CloseOpcions: CloseOpcions,
         cla: clasOp2,
         positionOpcions: 'positionOpcions2',       
       },
       {
         id:3,
         opcion1:'$500000-1000000', 
         opcion2:'$1100000-1500000',
         opcion3:'$1600000-2000000',
         opcion4:'$2100000-3000000',
         opcion5:'$3100000- ...',
         funcionn1: DisplayOpcion8,
         funcionn2: DisplayOpcion9,
         funcionn3: DisplayOpcion10,
         funcionn4: DisplayOpcion11,
         funcionn5: DisplayOpcion12,
         CloseOpcions: CloseOpcions,
         cla: clasOp3,
         positionOpcions: 'positionOpcions3',
       },
    ]
  

    
   
   ///al select nombre le paso el estado coloro y la funcion caco con corchetes por que representa codigo javascript dentro de jsx en cambio cuando se lo tengo que pasar al objeto lo hago sin corchetes
   return (
      <>
         <div className="div-para-activar-interfaz" onMouseOver={()=> {ActivInterfaz()} }  ></div>
         <div className="interfaz" onMouseLeave={()=>{DesactivInterfaz()}} style={{bottom:TopInterfaz}}>
            <img src={ImgInterfazTheme} className="fondo-interfaz"></img> 
            <div className="container-opcions flexbox"  >
            {
             DatosOpcions.map(({ id, margenAbajo, opcion1, opcion2, opcion3, opcion4, opcion5, funcionn1, funcionn2, funcionn3, funcionn4, funcionn5, CloseOpcions, cla, positionOpcions }) => <OpcionsSelect key={id}  margenAbajo={margenAbajo} opcion1={opcion1} opcion2={opcion2} opcion3={opcion3} opcion4={opcion4} opcion5={opcion5} funcionn1={funcionn1} funcionn2={funcionn2} funcionn3={funcionn3} funcionn4={funcionn4} funcionn5={funcionn5} CloseOpcions={CloseOpcions} cla={cla} positionOpcions={positionOpcions} />)  
            }
            </div>
            <div className="container-selects flexbox">
             <SelectNombre NameOpcion={NameSelect1} FuncionEstado={OpenOpcions} rotate={RotateIcon1} positionSelects='positionSelect1' />
             <SelectNombre NameOpcion={NameSelect2} FuncionEstado={OpenOpcions2} rotate={RotateIcon2} positionSelects='positionSelect2' /> 
             <SelectNombre NameOpcion={NameSelect3} FuncionEstado={OpenOpcions3} rotate={RotateIcon3} positionSelects='positionSelect3'/>
            </div>
         </div>
     </>
   );
}
  
  export default InterfazParaElegir; 

