import React, {useState} from 'react';
import CardGrupal from './conmponentes/cardCoches/cardGrupal'
import './estilos/globalEstilos.css'  
import './estilos/style.css'
import InterfazParaElegir from './conmponentes/interfazParaElegir/interfazParaElegir'
import MenuLateral from './conmponentes/menuLateral/menuLateral';
import DarkModeButton from './conmponentes/cardCoches/darkModeButton';



// Contexto para el dark mode
export const ContextTheme = React.createContext(
  );

//Este componente me contiene los 3 componente principales de la app que son, el contenedor de las cards de los autos y las cuotas, el menu lateral, y la interfaz con los select. Tambien me contiene el boton dark mode
const App = () => {


  //Segun que opcion de los select elija, estos estados me clasifican las cards que corresponden a dicha opcion, con uno de estos estados en true. Lo hice por que si las cards que tiene esa opcion son  pocas, la interfaz de los selects se muestra sin importar si hago scroll o no y sin importar si le paso el mouse por abajo de la pantalla. En cambio si son muchas las cards que se muestran, ahi si seria adecuado que el efecto de que el interfaz aparesca y desaparesca segun el scroll o si le paso el mouse este activado.

  const [flagOpInterfaz1, setflagOpInterfaz1] = useState(false);
  const [flagOpInterfaz2, setflagOpInterfaz2] = useState(false);
  const [flagOpInterfaz3, setflagOpInterfaz3] = useState(false); 

  
  
////////////////////////////////////////////////////////////

  // Este estado es para desplazar el contenedor de las cards hacia la derecha cuando se activa el menu lateral 
  const [stateMoveContainer, setstateMoveContainer] = useState(null);

  // Este estado es para desplazar el contenedor de las cards cuotas hacia la derecha cuando se activa el menu lateral 
  const [stateMoveContainer2, setstateMoveContainer2] = useState(null);

  

  //////// ESTADOS CON LOS NOMBRES DEL SELECTOR POR DEFECTO O OPCION ELEGIDA///////
  
  const [NameSelect1, setNameSelect1] = useState('Motor');
  const [NameSelect2, setNameSelect2] = useState('Tipo');
  const [NameSelect3, setNameSelect3] = useState('Precio');



  ///ESTADO QUE ME ACTIVA EL USEEFECT QUE ME CIERRA LAS CARDCUOTA ABIERTAS CUANDO HAGO UN CLICK EN UNA OPCION ////////

  const [ActivEffect, setActivEffect] = useState(0);
 


////ESTADOS PARA MANEJAR LOS DISPLAY DE CADA CARD CUANDO HAGO CLICK EN UNA OPCION DEL SELECT ///

  const[display1,setdisplay1] = useState(0);
  const[display2,setdisplay2] = useState(0);
  const[display3,setdisplay3] = useState(0);
  const[display4,setdisplay4] = useState(0);
  const[display5,setdisplay5] = useState(0);
  const[display6,setdisplay6] = useState(0);
  const[display7,setdisplay7] = useState(0);
  const[display8,setdisplay8] = useState(0);
  const[display9,setdisplay9] = useState(0);
  const[display10,setdisplay10] = useState(0);
  const[display11,setdisplay11] = useState(0);
  const[display12,setdisplay12] = useState(0);
  const[display13,setdisplay13] = useState(0);
  const[display14,setdisplay14] = useState(0);
  const[display15,setdisplay15] = useState(0);



  ///ESTADOS PARA CAMBIAR LA CLASE QUE HACE QUE SE ME EJECUTE EL KEYFRAM, CON EL EFECTO DE APARECER CON OPACITY DE CADA CARD CUANDO HAGO CLICK EN ALGUNA OPCION ///

  const [efectoCard1,setefectoCard1] = useState('contenedorCard');
  const [efectoCard2,setefectoCard2] = useState('contenedorCard');
  const [efectoCard3,setefectoCard3] = useState('contenedorCard');
  const [efectoCard4,setefectoCard4] = useState('contenedorCard');
  const [efectoCard5,setefectoCard5] = useState('contenedorCard');
  const [efectoCard6,setefectoCard6] = useState('contenedorCard');
  const [efectoCard7,setefectoCard7] = useState('contenedorCard');
  const [efectoCard8,setefectoCard8] = useState('contenedorCard');
  const [efectoCard9,setefectoCard9] = useState('contenedorCard');
  const [efectoCard10,setefectoCard10] = useState('contenedorCard');
  const [efectoCard11,setefectoCard11] = useState('contenedorCard');
  const [efectoCard12,setefectoCard12] = useState('contenedorCard');
  const [efectoCard13,setefectoCard13] = useState('contenedorCard');
  const [efectoCard14,setefectoCard14] = useState('contenedorCard');
  const [efectoCard15,setefectoCard15] = useState('contenedorCard');





///ESTAS FUNCIONES, CORRESPONDEN A LA FUNCIONALIDAD DE CADA OPCION DE LOS SELECT. LO QUE HACEN ES PONERME LAS CARDS QUE CORRESPONDEN A LA OPCION ELEJIDA EN BLOCK Y A LAS DEMAS EN NONE, ME CAMBIA EL ESTADO CON EL NOMBRE DEL SELECT DEJANDOME COMO NOMBRE LA OPCION ELEJIDA, ME CAMBIA LA CLASE QUE ME ACTIVA EL EFECTO KEYFRAM DE APARECER DE LAS CARDS, ME ACTIVA EL USEEFECTE QUE ME CIERRA LAS CARDS CUOTAS ABIERTAS, Y ME PONE EN TRUE EL ESTADO FlagOpInterfaz, CUYO FUNCIONAMIENTO EXPLIQUE MAS ARRIBA EN DONDE DECLARE E INICIALIZE EL ESTADO.///

 // En tamaño mobil no me entran los nombres de las opciones que se elijen dentro de los select, por eso cuando es tamaño mobil con esta funcion el nombre del select no cambia siepre es el mismo, en cambio, el nombre del select si cambia al nombre de la opcion elejida, cuando el tmaño de la pantalla no es mobil 
 const NameSelectMobil = (setNameselect, NameMobil, NameNoMobil ) => {
  
  let windowSize = window.innerWidth;

  (windowSize<700) ? setNameselect(NameMobil) : setNameselect(NameNoMobil);
 }

  const DisplayOpcion1 = () => {
  setdisplay1('block');
  setdisplay2('block');
  setdisplay3('block');
  setdisplay4('none');
  setdisplay5('block');
  setdisplay6('block');
  setdisplay7('block');
  setdisplay8('none');
  setdisplay9('none');
  setdisplay10('none');
  setdisplay11('none');
  setdisplay12('none');
  setdisplay13('none');
  setdisplay14('none');
  setdisplay15('none');
  NameSelectMobil(setNameSelect1, 'Motor', 'Motor 1.6');
  setNameSelect2('Tipo');
  setNameSelect3('Precio');
  setefectoCard2('classEfect');
  setefectoCard3('classEfect');
  setefectoCard5('classEfect');
  setefectoCard6('classEfect');
  setefectoCard7('classEfect');
  setflagOpInterfaz1(false);
  setflagOpInterfaz2(true);
  setflagOpInterfaz3(false);
  setActivEffect(+1);
  }
  
  const DisplayOpcion2 = () => {
    setdisplay1('none');
    setdisplay2('none');
    setdisplay3('none');
    setdisplay4('block');
    setdisplay5('none');
    setdisplay6('none');
    setdisplay7('none');
    setdisplay8('none');
    setdisplay9('block');
    setdisplay10('block');
    setdisplay11('none');
    setdisplay12('none');
    setdisplay13('none');
    setdisplay14('none');
    setdisplay15('none');
    NameSelectMobil(setNameSelect1, 'Motor', 'Motor 1.8');
    setNameSelect2('Tipo');
    setNameSelect3('Precio'); 
    setefectoCard4('classEfect');
    setefectoCard9('classEfect');
    setefectoCard10('classEfect');
    setflagOpInterfaz1(true);
    setflagOpInterfaz2(false);
    setflagOpInterfaz3(false);
    setActivEffect(+1);
  }
  
  const DisplayOpcion3 = () => {
    setdisplay1('none');
    setdisplay2('none');
    setdisplay3('none');
    setdisplay4('none');
    setdisplay5('none');
    setdisplay6('none');
    setdisplay7('none');
    setdisplay8('block');
    setdisplay9('none');
    setdisplay10('none');
    setdisplay11('block');
    setdisplay12('block');
    setdisplay13('block');
    setdisplay14('block');
    setdisplay15('block');
    NameSelectMobil(setNameSelect1, 'Motor', 'Motor 2.0');
    setNameSelect2('Tipo');
    setNameSelect3('Precio'); 
    setefectoCard8('classEfect');
    setefectoCard11('classEfect');
    setefectoCard12('classEfect');
    setefectoCard13('classEfect');
    setefectoCard14('classEfect');
    setefectoCard15('classEfect');
    setflagOpInterfaz1(false);
    setflagOpInterfaz2(true);
    setflagOpInterfaz3(false); 
    setActivEffect(+1);
  }
  
  const DisplayOpcion4 = () => {
    setdisplay1('block');
    setdisplay2('block');
    setdisplay3('block');
    setdisplay4('block');
    setdisplay5('block');
    setdisplay6('block');
    setdisplay7('block');
    setdisplay8('block');
    setdisplay9('none');
    setdisplay10('none');
    setdisplay11('none');
    setdisplay12('none');
    setdisplay13('none');
    setdisplay14('none');
    setdisplay15('none');
    NameSelectMobil(setNameSelect2, 'Tipo', 'Auto');
    setNameSelect1('Motor');
    setNameSelect3('Precio');
    setefectoCard1('classEfect');
    setefectoCard2('classEfect');
    setefectoCard3('classEfect');
    setefectoCard4('classEfect');
    setefectoCard5('classEfect');
    setefectoCard6('classEfect');
    setefectoCard7('classEfect');
    setefectoCard8('classEfect'); 
    setflagOpInterfaz1(false);
    setflagOpInterfaz2(false);
    setflagOpInterfaz3(true);
    setActivEffect(+1);
  }
  
  const DisplayOpcion5 = () => {
    setdisplay1('none');
    setdisplay2('none');
    setdisplay3('none');
    setdisplay4('none');
    setdisplay5('none');
    setdisplay6('none');
    setdisplay7('none');
    setdisplay8('none');
    setdisplay9('block');
    setdisplay10('block');
    setdisplay11('none');
    setdisplay12('none');
    setdisplay13('none');
    setdisplay14('none');
    setdisplay15('none');
    NameSelectMobil(setNameSelect2, 'Tipo', 'Monovolumen');
    setNameSelect1('Motor');
    setNameSelect3('Precio'); 
    setefectoCard9('classEfect');
    setefectoCard10('classEfect');
    setflagOpInterfaz1(true);
    setflagOpInterfaz2(false);
    setflagOpInterfaz3(false);
    setActivEffect(+1);
  }
  
  const DisplayOpcion6 = () => {
    setdisplay1('none');
    setdisplay2('none');
    setdisplay3('none');
    setdisplay4('none');
    setdisplay5('none');
    setdisplay6('none');
    setdisplay7('none');
    setdisplay8('none');
    setdisplay9('none');
    setdisplay10('none');
    setdisplay11('block');
    setdisplay12('block');
    setdisplay13('none');
    setdisplay14('none');
    setdisplay15('none');
    NameSelectMobil(setNameSelect2, 'Tipo', 'Suv');
    setNameSelect1('Motor');
    setNameSelect3('Precio');
    setefectoCard11('classEfect');
    setefectoCard12('classEfect');
    setflagOpInterfaz1(true);
    setflagOpInterfaz2(false);
    setflagOpInterfaz3(false);
    setActivEffect(+1);
  }
  
  const DisplayOpcion7 = () => {
    setdisplay1('none');
    setdisplay2('none');
    setdisplay3('none');
    setdisplay4('none');
    setdisplay5('none');
    setdisplay6('none');
    setdisplay7('none');
    setdisplay8('none');
    setdisplay9('none');
    setdisplay10('none');
    setdisplay11('none');
    setdisplay12('none');
    setdisplay13('block');
    setdisplay14('block');
    setdisplay15('block');
    NameSelectMobil(setNameSelect2, 'Tipo', 'Pickup');
    setNameSelect1('Motor');
    setNameSelect3('Precio');
    setefectoCard13('classEfect');
    setefectoCard14('classEfect');
    setefectoCard15('classEfect');
    setflagOpInterfaz1(true);
    setflagOpInterfaz2(false);
    setflagOpInterfaz3(false);
    setActivEffect(+1);
  }
  
  const DisplayOpcion8 = () => {
    setdisplay1('block');
    setdisplay2('block');
    setdisplay3('block');
    setdisplay4('none');
    setdisplay5('none');
    setdisplay6('none');
    setdisplay7('none');
    setdisplay8('none');
    setdisplay9('none');
    setdisplay12('none');
    setdisplay13('none');
    setdisplay14('none');
    setdisplay15('none');
    NameSelectMobil(setNameSelect3, 'Precio', '$500k-$1.000k');
    setNameSelect1('Motor');
    setNameSelect2('Tipo');
    setefectoCard1('classEfect');
    setefectoCard2('classEfect');
    setefectoCard3('classEfect');
    setflagOpInterfaz1(true);
    setflagOpInterfaz2(false);
    setflagOpInterfaz3(false);
    setActivEffect(+1);
  }
  
  const DisplayOpcion9 = () => {
    setdisplay1('none');
    setdisplay2('none');
    setdisplay3('none');
    setdisplay4('none');
    setdisplay5('none');
    setdisplay6('block');
    setdisplay7('block');
    setdisplay8('none');
    setdisplay9('block');
    setdisplay10('none');
    setdisplay11('none');
    setdisplay12('none');
    setdisplay13('none');
    setdisplay14('none');
    setdisplay15('none'); 
    NameSelectMobil(setNameSelect3, 'Precio', '$1.100k-$1.500');
    setNameSelect1('Motor');
    setNameSelect2('Tipo');
    setefectoCard6('classEfect');
    setefectoCard7('classEfect');
    setefectoCard9('classEfect'); 
    setflagOpInterfaz1(true);
    setflagOpInterfaz2(false);
    setflagOpInterfaz3(false);
    setActivEffect(+1);
  }

  const DisplayOpcion10 = () => {
    setdisplay1('none');
    setdisplay2('none');
    setdisplay3('none');
    setdisplay4('block');
    setdisplay5('none');
    setdisplay6('none');
    setdisplay7('none');
    setdisplay8('none');
    setdisplay9('none');
    setdisplay10('block');
    setdisplay11('none');
    setdisplay12('block');
    setdisplay13('none');
    setdisplay14('none');
    setdisplay15('none');
    NameSelectMobil(setNameSelect3, 'Precio', '$1.600k-$2.000')
    setNameSelect1('Motor');
    setNameSelect2('Tipo');
    setefectoCard4('classEfect');
    setefectoCard10('classEfect');
    setefectoCard12('classEfect');
    setflagOpInterfaz1(true);
    setflagOpInterfaz2(false);
    setflagOpInterfaz3(false);
    setActivEffect(+1);
  }
  
  const DisplayOpcion11 = () => {
    setdisplay1('none');
    setdisplay2('none');
    setdisplay3('none');
    setdisplay4('none');
    setdisplay5('block');
    setdisplay6('none');
    setdisplay7('none');
    setdisplay8('none');
    setdisplay9('none');
    setdisplay10('none');
    setdisplay11('block');
    setdisplay12('none');
    setdisplay13('block');
    setdisplay14('none');
    setdisplay15('none');
    NameSelectMobil(setNameSelect3, 'Precio', '$2.100k-$3.000');
    setNameSelect1('Motor');
    setNameSelect2('Tipo');
    setefectoCard5('classEfect');
    setefectoCard11('classEfect');
    setefectoCard13('classEfect');
    setflagOpInterfaz1(true);
    setflagOpInterfaz2(false);
    setflagOpInterfaz3(false);
    setActivEffect(+1);
  }
  
  const DisplayOpcion12 = () => {
    setdisplay1('none');
    setdisplay2('none');
    setdisplay3('none');
    setdisplay4('none');
    setdisplay5('none');
    setdisplay6('none');
    setdisplay7('none');
    setdisplay8('block');
    setdisplay9('none');
    setdisplay10('none');
    setdisplay11('none');
    setdisplay12('none');
    setdisplay13('none');
    setdisplay14('block');
    setdisplay15('block');
    NameSelectMobil(setNameSelect3, 'Precio', '$3.100k-...');
    setNameSelect1('Motor');
    setNameSelect2('Tipo');
    setefectoCard8('classEfect');
    setefectoCard14('classEfect');
    setefectoCard15('classEfect')
    setflagOpInterfaz1(true);
    setflagOpInterfaz2(false);
    setflagOpInterfaz3(false);
    setActivEffect(+1);
  }

  ///PUSE ESTOS 3 ESTADOS EN UN ARRAY PARA PASARLOS POR PROPS Y LUEGO UTILIZARLOS EN UN FOR DENTRO DEL COMPONENTE InterfazParaElegir ///
  let flagOpInterfaz = [flagOpInterfaz1,flagOpInterfaz2,flagOpInterfaz3];



  /// ESTADOS PARA MANEJAR EL DARKMODE ///

 // Estados para manejar el icono del boton del dark mode o light mode segun corresponda y para el texto que esta debajo del icono.
  const [IconThemeButton, setIconThemeButton] = useState('imagenes/night.png');
  const [TextThemeButton, setTextThemeButton] = useState('Dark mode');

  // Este estado es para cambiar la imagen de la interfaz de los selects, segun que modo este activado
  const [ImgInterfazTheme, setImgInterfazTheme] = useState('imagenes/fondo15.jpg');

  // Estados para manejar los colores de los elementos. Estos los distribuyo en los elementos que quiero cambiar de color cuando hago click en el boton de cambiar el mode
  const [NightBoxShadow, setNightBoxShadow] = useState(null);
  const [ColorThemeWhite, setColorThemeWhite] = useState(null);
  const [ColorThemeBlack, setColorThemeBlack] = useState(null);
  const [ColorThemeGrey, setColorThemeGrey] = useState(null);

  return (
    <>

      <ContextTheme.Provider value={{NightBoxShadow, setNightBoxShadow, ColorThemeWhite, setColorThemeWhite, ColorThemeBlack, setColorThemeBlack, ColorThemeGrey, setColorThemeGrey, IconThemeButton, setIconThemeButton, TextThemeButton, setTextThemeButton, ImgInterfazTheme, setImgInterfazTheme}}>
      <div style={{background:ColorThemeBlack}}>
      <DarkModeButton/>
      <MenuLateral opcion1="Home" opcion2="Catalogo" opcion3="Login" opcion4="Soporte" opcion5="Contacto" setstateMoveContainer={setstateMoveContainer} setstateMoveContainer2={setstateMoveContainer2}/>    
      
      <CardGrupal propMoveContainer={stateMoveContainer} propMoveContainer2={stateMoveContainer2} ActivEffect={ActivEffect} display1={display1} display2={display2} display3={display3} display4={display4} display5={display5} display6={display6} display7={display7} display8={display8} display9={display9} display10={display10} display11={display11} display12={display12} display13={display13} display14={display14} display15={display15} efectoCard1={efectoCard1} efectoCard2={efectoCard2} efectoCard3={efectoCard3} efectoCard4={efectoCard4} efectoCard5={efectoCard5} efectoCard6={efectoCard6} efectoCard7={efectoCard7} efectoCard8={efectoCard8} efectoCard9={efectoCard9} efectoCard10={efectoCard10} efectoCard11={efectoCard11} efectoCard12={efectoCard12} efectoCard13={efectoCard13} efectoCard14={efectoCard14} efectoCard15={efectoCard15} />
      
      <InterfazParaElegir flagOpInterfaz={flagOpInterfaz} DisplayOpcion1={DisplayOpcion1} DisplayOpcion2={DisplayOpcion2}       DisplayOpcion3={DisplayOpcion3} DisplayOpcion4={DisplayOpcion4} DisplayOpcion5={DisplayOpcion5} DisplayOpcion6={DisplayOpcion6} DisplayOpcion7={DisplayOpcion7} DisplayOpcion8={DisplayOpcion8} DisplayOpcion9={DisplayOpcion9} DisplayOpcion10={DisplayOpcion10} DisplayOpcion11={DisplayOpcion11} DisplayOpcion12={DisplayOpcion12}
      NameSelect1={NameSelect1} NameSelect2={NameSelect2} NameSelect3={NameSelect3} />
      </div>
    </ContextTheme.Provider>
    
    </>
    
  );
}

export default App;








